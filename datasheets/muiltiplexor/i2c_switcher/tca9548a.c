#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"




HAL_StatusTypeDef TCA9548A_ReadDataByte(uint8_t reg, uint8_t *val)
{
	HAL_StatusTypeDef status;
	status = HAL_I2C_Mem_Read(&hi2c2, TCA9548A_LAddr000, reg, I2C_MEMADD_SIZE_8BIT, (uint8_t *)val, 2, 10);
	return status;
}

HAL_StatusTypeDef TCA9548A_WriteDataByte(uint8_t reg, uint8_t val)
{
	HAL_StatusTypeDef status;
	status = HAL_I2C_Mem_Write(&hi2c2, TCA9548A_LAddr000, reg, I2C_MEMADD_SIZE_8BIT, &val, 2, 10);
	return status;
}
// Библиотека для работы с протоколом I2C (порты A5/SCL и A4/SDA)
#include <Wire.h>

#define TCAADDR 0x70
// упрощеный I2C адрес нашего гироскопа/акселерометра MPU-6050.
const int MPU_addr = 0x68;
// переменные для хранения данных возвращаемых прибором.
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
double CompensatorZ, CompensatorX, CompensatorY;
double CompensatorZarray[7], CompensatorXarray[7], CompensatorYarray[7];
unsigned long timer = 0;

void tcaselect(uint8_t i);
void giroscop_setup(uint8_t adr);
void Calc_CompensatorZ(uint8_t adr, unsigned long mill_sec);
void Data_mpu6050(uint8_t adr);
void setup() {

  Serial.begin(115200);
  Serial.println();

  /* Enable I2C */
  Wire.begin();//(22,23); //SDA //SCL

  // BNO055 clock stretches for 500us or more!
#ifdef ESP8266
  Wire.setClockStretchLimit(1000); // Allow for 1000us of clock stretching
#endif

  giroscop_setup(2);
  giroscop_setup(7);
  Calc_CompensatorZ(2, 2000);
  CompensatorZarray[2]=CompensatorZ; CompensatorXarray[2]=CompensatorX; CompensatorYarray[2]=CompensatorY;
  Calc_CompensatorZ(7, 2000);
  CompensatorZarray[7]=CompensatorZ; CompensatorXarray[7]=CompensatorX; CompensatorYarray[7]=CompensatorY;
}

void loop()
{
  Data_mpu6050(2);
  Serial.println("  MPU6050 on 2 adress");
  Serial.print("Accel X = "); Serial.print(AcX);
  Serial.print("  Accel Y = "); Serial.print(AcY);
  Serial.print("  Accel Z = "); Serial.println(AcZ);
  Serial.print("angular speed X = "); Serial.print(GyX);
  Serial.print(" angular speed Y = "); Serial.print(GyY);
  Serial.print(" angular speed Z = "); Serial.println(GyZ);
  Serial.println();
  Data_mpu6050(7);

  Serial.println("  MPU6050 on 7 adress");
  Serial.print("Accel X = "); Serial.print(AcX);
  Serial.print("  Accel Y = "); Serial.print(AcY);
  Serial.print("  Accel Z = "); Serial.println(AcZ);
  Serial.print("angular speed X = "); Serial.print(GyX);
  Serial.print(" angular speed Y = "); Serial.print(GyY);
  Serial.print(" angular speed Z = "); Serial.println(GyZ);
  Serial.println();
  delay(1000);
}

//Функция мультиплексирует/переключает адреса для обмена
void tcaselect(uint8_t i) {
  if (i > 7) return;

  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();
}
/////////////////////////////////////////////////////////////////////////
//// Считывание данных с mpu6050
/////////////////////////////////////////////////////////////////////////
void Data_mpu6050(uint8_t adr)
{
  tcaselect(adr); //Выбор переключаемого адреса
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  //Готовим для чтения регистры  с адреса 0x3B.
  Wire.endTransmission(false);
  // Запрос 14 регистров.
  Wire.requestFrom(MPU_addr, 14, true);
  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcX = Wire.read() << 8 | Wire.read();
  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcY = Wire.read() << 8 | Wire.read();
  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  AcZ = Wire.read() << 8 | Wire.read();
  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  Tmp = Wire.read() << 8 | Wire.read();
  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyX = Wire.read() << 8 | Wire.read();
  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyY = Wire.read() << 8 | Wire.read();
  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
  GyZ = Wire.read() << 8 | Wire.read();
}
/////////////////////////////////////////////////////////////////////////
/// Запуск гироскопа
/////////////////////////////////////////////////////////////////////////
void giroscop_setup(uint8_t adr)
{
  tcaselect(adr); //Выбор переключаемого адреса

  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // Производим запись в регистр энергосбережения MPU-6050
  Wire.write(0);     // устанавливем его в ноль
  Wire.endTransmission(true);
  CompensatorZ = 0;
  CompensatorX = 0;
  CompensatorY = 0;
}

/////////////////////////////////////////////////////////////////////////
// Компенсация нуля гироскопа
void Calc_CompensatorZ(uint8_t adr, unsigned long mill_sec)
{
  //tcaselect(adr); //Выбор переключаемого адреса
  float i = 0;
  CompensatorZ = 0;
  CompensatorX = 0;
  CompensatorY = 0;
  timer = millis();
  unsigned long endtime = millis() + mill_sec;
  while (endtime > timer) {
    timer = millis();
    Data_mpu6050(adr);
    CompensatorZ += (float)(GyZ);
    CompensatorX += (float)(GyX);
    CompensatorY += (float)(GyY);
    delay(2);
    i++;
  }
  CompensatorZ /= i;
  CompensatorX /= i;
  CompensatorY /= i;

}

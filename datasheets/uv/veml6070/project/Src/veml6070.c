/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

#include "veml6070.h"

static short previous_value = 0;

//-------------------------------------------------------------------------------
uint8_t VEML6070_Init(void) 
{
    /* Conf veml6070 */
    uint8_t InitialConf = 0x02; // Reservd bit 00XXXX1X 
    uint8_t Configuration = InitialConf | SD_DISABLE | IT_0_5 | ACK_THD_102 | ACK_DISABLE;
    
    Check_ARA();
    
    HAL_I2C_Master_Transmit(&VEML6070_I2C, VEML6070_I2C_ADDR,&Configuration, 1,VEML6070_I2C_TIMEOUT);
return 1;	
    
}
//------------------------------------------------------------------------------
short veml6070Read(void)
{
    uint8_t msb;
    uint8_t lsb;
    
    if (HAL_OK != HAL_I2C_Master_Receive(&VEML6070_I2C, VEML6070_I2C_ADDR_LSB, &lsb, 1, VEML6070_I2C_TIMEOUT))
    {
        Check_ARA();
        return previous_value;
    }
    
    if (HAL_OK != HAL_I2C_Master_Receive(&VEML6070_I2C, VEML6070_I2C_ADDR_MSB, &msb, 1, VEML6070_I2C_TIMEOUT))
    {
        Check_ARA();
        return previous_value;
    }
    
    previous_value = (msb << 8) | lsb;
    return previous_value;
}
//------------------------------------------------------------------------------
void Check_ARA(void)
{
    uint8_t i;
    uint8_t buffer;
    buffer = 0;
    
    for( i = 0; i < 200 ; i++)
    {
        HAL_I2C_Master_Receive(&VEML6070_I2C, VEML6070_I2C_ADDR_ARA,&buffer, 1, 1);
    }
    
}

//------------------------------------------------------------------------------

void itoa (int n, char s[]) 
{ 
    int i, sign,j; 
    char c;
    if ((sign = n) < 0) 
        n = -n; 
    i = 0; 
    do { 
        s[i++] = n % 10 + '0'; 
    } while ((n /= 10) > 0); 
    if (sign < 0) 
        s[i++] = '-'; 
    s[i] = '\0'; 
    j = i-1;
    for (i = 0; i < j; j=j ) 
    {
      c = s[i];
    s[i] = s[j];
    s[j] = c; 
    i++; j--;
    }
} 
//------------------------------------------------------------------------------

/* CODE END Public functions */

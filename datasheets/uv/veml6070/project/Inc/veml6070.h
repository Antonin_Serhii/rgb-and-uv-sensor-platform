#ifndef _VEML607_H_
#define _VEML607_H_

#include <string.h>
#include <stdlib.h>

/* CODE BEGIN Private defines */

/* Masks */
#define SD_ANABLE 0x01   // -------1
#define SD_DISABLE 0x00  // -------0
#define IT_0_5 0x00      // ----00--
#define IT_1 0x04        // ----01--
#define IT_2 0x08        // ----10--
#define IT_4 0x12        // ----11--
#define ACK_THD_102 0x00 // ---0----
#define ACK_THD_145 0x16 // ---1----
#define ACK_ANABLE 0x32  // --1-----
#define ACK_DISABLE 0x00 // --0-----


/* I2C settings */
#define VEML6070_I2C              hi2c2

/* I2C address */

#define VEML6070_I2C_ADDR               0x70
#define VEML6070_I2C_ADDR_ARA           0x30
#define VEML6070_I2C_ADDR_FOR_CONF      0x70
#define VEML6070_I2C_ADDR_LSB           0x71
#define VEML6070_I2C_ADDR_MSB           0x73

#define VEML6070_I2C_TIMEOUT	        1000

/* CODE END Private defines */

/* CODE BEGIN Private typedefs */

/* CODE END Private typedefs */

/* CODE BEGIN External variables */
extern I2C_HandleTypeDef VEML6070_I2C;
/* CODE END External variables */

/* CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
short veml6070Read(void);
void Check_ARA(void);
uint8_t VEML6070_Init(void);
void itoa (int n, char *pbuff);
/* CODE END PFP */

/* CODE BEGIN DEMO */

/* CODE END DEMO */

#endif /* VEML607_H_ */

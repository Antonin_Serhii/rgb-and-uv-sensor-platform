

#include "VEML6070.h"


void VEMLClass::init() {
	VEML6070_init();
	delay(1);
	VEML6040_init();
	delay(1);
}

void VEMLClass::VEML6070_init() {
	VEML6070_write(0x02);
}

void VEMLClass::VEML6070_write(uns8 cmd) {
	Wire.beginTransmission(VEML6070_ADDR);
	Wire.write(cmd);
	Wire.endTransmission();
	VEML6070_command = cmd;
}

uns16 VEMLClass::getUV() {
	VEML6070_write(VEML6070_command);
	uns8 msb, lsb;
	Wire.requestFrom(VEML6070_ADDR+1, 1);
	if (Wire.available()) {
		msb = Wire.read();
	}
	Wire.requestFrom(VEML6070_ADDR+0, 1);
	delay(1);
	if (Wire.available()) {
		lsb = Wire.read();
	}
	return (msb << 8) | lsb;
}

void VEMLClass::VEML6070_setIT(uns8 it) {
	it = (VEML6070_command & 0xF3) | (it << 2);
	VEML6070_write(it);
	VEML6070_command = it;
	delay(1);
}


VEMLClass VEML6070;
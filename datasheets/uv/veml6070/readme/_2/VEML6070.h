#ifndef __VEML6070_h
#define	__VEML6070_h


//UV Sensor VEML6070 Address
#define VEML6070_ADDR	0x38 //i2c Address UV sensor

	unsigned char VEML6070_command; //Hold previous byte written
	void VEML6070_init(); //Initialize VEML6070 = 0
	void VEML6070_write(unsigned char cmd); //Write command to the VEML6070

	void init(); //VEML6070
	unsigned int getUV(); //Reads UV measurement
	

	void VEML6070_setIT(unsigned char it);  // Set integration time for VEML6070 measurement. 0 = 1/2T, 1 = T, 2 = 2T, 3 = 4T
	

};

extern VEMLClass VEML6070;

#endif
#include "stm32f10x.h"

#define APDS9960_I2C_ADDR       0x39
#define APDS9960_ATIME          0x81
#define APDS9960_CONTROL        0x8F
#define APDS9960_ENABLE         0x80

#define APDS9960_CDATAL         0x94
#define APDS9960_CDATAH         0x95
#define APDS9960_RDATAL         0x96
#define APDS9960_RDATAH         0x97
#define APDS9960_GDATAL         0x98
#define APDS9960_GDATAH         0x99
#define APDS9960_BDATAL         0x9A
#define APDS9960_BDATAH         0x9B

/* Bit fields */
#define APDS9960_PON            0x01
#define APDS9960_AEN            0x02
#define APDS9960_PEN            0x04
#define APDS9960_WEN            0x08
#define APSD9960_AIEN           0x10
#define APDS9960_PIEN           0x20
#define APDS9960_GEN            0x40
#define APDS9960_GVALID         0x01

/* ALS Gain (AGAIN) values */
#define AGAIN_1X                0
#define AGAIN_4X                1
#define AGAIN_16X               2
#define AGAIN_64X               3

#define DEFAULT_ATIME           219     // 103ms
#define DEFAULT_AGAIN           AGAIN_4X

uint8_t Colour_tmpL = 0;
uint8_t Colour_tmpH = 0;
uint16_t Colour_Clear = 0;
uint16_t Colour_Red = 0;
uint16_t Colour_Green = 0;
uint16_t Colour_Blue = 0;

//-----------------------------------------------------------------------

void I2C1_init(void)
{

    I2C_InitTypeDef I2C_InitStructure;
  GPIO_InitTypeDef  GPIO_InitStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB| RCC_APB2Periph_AFIO , ENABLE);

    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    I2C_StructInit(&I2C_InitStructure);
    I2C_InitStructure.I2C_ClockSpeed = 100000;
    I2C_InitStructure.I2C_OwnAddress1 = 0x01;
    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
    I2C_Init(I2C1, &I2C_InitStructure);
    I2C_Cmd(I2C1, ENABLE);

}

//-----------------------------------------------------------------------

uint8_t i2c1_read(uint8_t addr)
{
    uint8_t data;
    while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
    I2C_GenerateSTART(I2C1, ENABLE);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
    I2C_Send7bitAddress(I2C1, APDS9960_I2C_ADDR<<1, I2C_Direction_Transmitter);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    I2C_SendData(I2C1, addr);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_GenerateSTART(I2C1, ENABLE);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
    I2C_Send7bitAddress(I2C1, APDS9960_I2C_ADDR<<1, I2C_Direction_Receiver);
    while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_BYTE_RECEIVED));
    data = I2C_ReceiveData(I2C1);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED));
    I2C_AcknowledgeConfig(I2C1, DISABLE);
    I2C_GenerateSTOP(I2C1, ENABLE);
    while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
    return data;
}

//-----------------------------------------------------------------------

void i2c1_write(uint8_t addr, uint8_t data)
{
    I2C_GenerateSTART(I2C1, ENABLE);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
    I2C_Send7bitAddress(I2C1, APDS9960_I2C_ADDR<<1, I2C_Direction_Transmitter);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    I2C_SendData(I2C1, addr);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C1, data);
    while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_GenerateSTOP(I2C1, ENABLE);
    while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)) {};

}

//-----------------------------------------------------------------------

int main()
{

    I2C1_init();

    i2c1_write(APDS9960_ATIME, DEFAULT_ATIME);
    i2c1_write(APDS9960_CONTROL, DEFAULT_AGAIN);
    i2c1_write(APDS9960_ENABLE, (APDS9960_PON | APDS9960_AEN));

  while (1)
  {

            Colour_Clear = 0;
            Colour_Red = 0;
            Colour_Green = 0;
            Colour_Blue = 0;

        //_________________________________________________________________________
        // Ambient Light Recognize:

            Colour_tmpL = i2c1_read(APDS9960_CDATAL);
            Colour_tmpH = i2c1_read(APDS9960_CDATAH);

            Colour_Clear = (Colour_tmpH << 8) + Colour_tmpL;

        //_________________________________________________________________________
        // RED color Recognize:

            Colour_tmpL = i2c1_read(APDS9960_RDATAL);
            Colour_tmpH = i2c1_read(APDS9960_RDATAH);

            Colour_Red = (Colour_tmpH << 8) + Colour_tmpL;

        //_________________________________________________________________________
        // GREEN color Recognize:

            Colour_tmpL = i2c1_read(APDS9960_GDATAL);
            Colour_tmpH = i2c1_read(APDS9960_GDATAH);

            Colour_Green = (Colour_tmpH << 8) + Colour_tmpL;

        //_________________________________________________________________________
        // BLUE color Recognize:

            Colour_tmpL = i2c1_read(APDS9960_BDATAL);
            Colour_tmpH = i2c1_read(APDS9960_BDATAH);

            Colour_Blue = (Colour_tmpH << 8) + Colour_tmpL;

    }

}
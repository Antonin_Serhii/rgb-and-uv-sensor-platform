/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

#include "base.h"
#include "ssd1306.h"
#include "APDS9960.h"



void ssd1306_Display_Init(void);
void ssd1306_Display_Out(void);

static uint32_t TimerBlick;



void TaskInit(void const * argument)
{

    char buf[6]="0";
    uint16_t Clear = 0;
    uint16_t Red = 0;
    uint16_t Green = 0;
    uint16_t Blue = 0;
    SemInit();
    
#if 1    
    ssd1306_Init();
    //ssd1306_FlipScreenVertically();
    ssd1306_Clear();
    ssd1306_SetColor(White);
    ssd1306_Display_Init();
    APDS9960_init();
#endif 
    
    
#if 1
    HAL_Delay(50);//for init sensor after power_on

#endif
    
    
    
    
    
    
    for(;;)
    {
        
        
        if(!((++TimerBlick)%10))
        {
            TimerBlick = 0;
            HAL_GPIO_TogglePin(PC13LD_GPIO_Port, PC13LD_Pin);
        }
        ssd1306_Clear();
        
        	Colour_Clear();
            Red = Colour_Red();
            Green = Colour_Green();
            Blue = Colour_Blue();
            
            ssd1306_SetCursor(30, 17);
            ssd1306_WriteString("R", Font_7x10);
			itoa(Red, buf);
			ssd1306_SetCursor(50, 17);
            ssd1306_WriteString(buf, Font_7x10);
            
            ssd1306_SetCursor(30, 27);
            ssd1306_WriteString("G", Font_7x10);
            itoa(Green, buf);
			ssd1306_SetCursor(50, 27);
            ssd1306_WriteString(buf, Font_7x10);
            
            ssd1306_SetCursor(30, 37);
            ssd1306_WriteString("B", Font_7x10);
            itoa(Blue, buf);
			ssd1306_SetCursor(50, 37);
			ssd1306_WriteString(buf, Font_7x10);
        
        ssd1306_UpdateScreen();
     //   ssd1306_Display_Out();
        
        

        osDelay(1);
    } 
}









//***************************************************************************

void ssd1306_Display_Init(void)
{
#if 1 //demo I2C_LCD_SSD1306
    //drawLines();
    //HAL_Delay(500);
    //ssd1306_Clear();
    
    //drawRect();
    //HAL_Delay(500);
    //ssd1306_Clear();
    
    //fillRect();
    //HAL_Delay(500);
    //ssd1306_Clear();
    
    //drawCircle();
    //HAL_Delay(500);
    //ssd1306_Clear();
    
    //for(uint8_t i = 0; i < 100; i++)
    //{
    //    drawProgressBarDemo(i);
    //    HAL_Delay(5);
    //    ssd1306_Clear();
    //}
    
    ssd1306_DrawRect(0, 0, ssd1306_GetWidth(), ssd1306_GetHeight());
    ssd1306_SetCursor(25, 22);
    ssd1306_WriteString("SSD1306", Font_11x18);
    ssd1306_UpdateScreen();
    ssd1306_Clear();
    HAL_Delay(1000);
    
    
    ssd1306_SetCursor(25, 1);
    ssd1306_WriteString("UV-Luxmeter", Font_7x10);
    ssd1306_SetCursor(0, 20);
    ssd1306_WriteString("semenenko_myk", Font_7x10);
    ssd1306_SetCursor(45, 34);
    ssd1306_WriteString("@ukr.net", Font_7x10);
    
    ssd1306_UpdateScreen();
    ssd1306_Clear();
    HAL_Delay(3000);
    
    
#endif 
}


void ssd1306_Display_Out(void)
{
    
#if 1
    ssd1306_Clear();
    
    ssd1306_SetCursor(25, 2);
    ssd1306_WriteString("UV Luxmeter", Font_7x10);
    
    
    
    
    
    ssd1306_UpdateScreen();
#endif
    
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{   
    uint8_t A; 
    A = 0;
    
   // COM_ON_Pin;
       // REGIME_Pin;
    
    UNUSED(GPIO_Pin); 
}

#include "base.h"


void TaskButtons(void const * argument)
{
    osDelay(1);
	
    for(;;)
    {
         
        if( button_OnHandle != NULL ){
            if(xSemaphoreTake( button_OnHandle, ( TickType_t ) 10 ) == pdTRUE ){
                // xSemaphoreGive( sensor_OnHandle );
                ButtonOnEvent(E_BUTTON_DWN);
                taskYIELD();
            }
        }
        
        if( button_SlctHandle != NULL ){
            if(xSemaphoreTake( button_SlctHandle, ( TickType_t ) 10 ) == pdTRUE ){
                ButtonSlctEvent();
                taskYIELD();
            }
        }
		
        osDelay(1);
    }
}
#ifndef TCA9548A_H
#define TCA9548A_H 

#include "stm32f1xx_hal.h"

#include "base.h"

#define TCA9548A_Addr000 0x70 // 112 // basic address A0, A1, A2 are grounded
#define TCA9548A_Addr001 0x71 // 113
#define TCA9548A_Addr010 0x72 // 114
#define TCA9548A_Addr011 0x73 // 115
#define TCA9548A_Addr100 0x74 // 116
#define TCA9548A_Addr101 0x75 // 117
#define TCA9548A_Addr110 0x76 // 118
#define TCA9548A_Addr111 0x77 // 119

// to use HAL_I2C_MEM_Read/Write functions do use left shifted basic address
#define TCA9548A_LAddr000 0xE0 // TCA9548A_Addr000<<1
#define TCA9548A_LAddr001 0xE2 // 
#define TCA9548A_LAddr010 0xE4 // 
#define TCA9548A_LAddr011 0xE6 // 
#define TCA9548A_LAddr100 0xE8 // 
#define TCA9548A_LAddr101 0xEA // 
#define TCA9548A_LAddr110 0xEC // 
#define TCA9548A_LAddr111 0xEE // 


HAL_StatusTypeDef TCA9548A_ReadDataByte(uint8_t reg, uint8_t *val);
HAL_StatusTypeDef TCA9548A_WriteDataByte(uint8_t reg, uint8_t val);

#endif

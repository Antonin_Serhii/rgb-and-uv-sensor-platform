#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

#include "base.h"

extern osSemaphoreId UART_RXHandle;
extern osSemaphoreId UART_TXHandle;
extern osSemaphoreId COM_RXHandle;
extern osSemaphoreId COM_TXHandle;


void SemInit(void)
{
    /* ������� �� ������ ������� - ���������� */
    if(xSemaphoreTake(UART_RXHandle, 2/portTICK_PERIOD_MS) == pdTRUE){ }
    
     /* ������� �� ������ ������� - ���������� */
    if(xSemaphoreTake(UART_TXHandle, 2/portTICK_PERIOD_MS) == pdTRUE){ }
    
        /* ������� �� ������ ������� - ���������� */
    if(xSemaphoreTake(COM_RXHandle, 2/portTICK_PERIOD_MS) == pdTRUE){ }
    
     /* ������� �� ������ ������� - ���������� */
    if(xSemaphoreTake(COM_TXHandle, 2/portTICK_PERIOD_MS) == pdTRUE){ }
    
}
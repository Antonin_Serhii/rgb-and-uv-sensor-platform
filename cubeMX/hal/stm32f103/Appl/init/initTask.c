#include "base.h"

static void svSemInit(void); // init semaphores beacause they are run after system init

#if 0
static char buf[6];
static size_t Clear;
static size_t Red;
static size_t Green;
static size_t Blue;
static size_t UV;
static size_t testTimer;
#endif


void TaskInit(void const * argument)
{
    svSemInit();    // init semaphores beacause they are run after system init
    drawDemo();     // init LCD
    
#if 0
    Clear = 0;
    Red = 0;
    Green = 0;
    Blue = 0;
    UV = 0;
#endif
    
    
    
    osDelay(5);
    
    
    for(;;)
    {
        
        

        
        
        osDelay(1);
    }
    
}




//
static void svSemInit(void) // init semaphores beacause they are run after system init
{
    // init semaphores beacause they are run after system init
    if(xSemaphoreTake(sensor_OnHandle, 2/portTICK_PERIOD_MS) == pdTRUE){ }
    
    // init semaphores beacause they are run after system init
    if(xSemaphoreTake(button_OnHandle, 2/portTICK_PERIOD_MS) == pdTRUE){ }
    
    // init semaphores beacause they are run after system init
    if(xSemaphoreTake(button_SlctHandle, 2/portTICK_PERIOD_MS) == pdTRUE){ }
}
#ifndef _MENU_H
#define _MENU_H

#include "fonts.h"

//
typedef enum E_MENU // Name of pages of menu
{
	E_MAIN,
	E_DIAGNOSTIC,
	E_CALIBRATION,
	E_SERVICE,
	E_SENSORS,
	E_FIRMWARE,
	E_SENSORS_1,
	E_SENSORS_2,
	E_SENSORS_3,
	E_SENSORS_4,
	E_SENSORS_5,
	E_SENSORS_6,
	E_ADC_GAIN,
	E_MEASUREMENT_TIME,
	E_RGBC,
	E_SENSORS_CONFIG,
	
	E_MENU_END
} teMenu;

typedef enum E_BUTTONS //
{
	E_BUTTON_UP,
	E_BUTTON_DWN,
	E_BUTTON_LFT,
	E_BUTTON_RGHT,
	
	E_BUTTON_SELECT,
	
	E_BUTTON_END
} teButtons;

//
typedef struct DisplayConfig // Template for printing the commands or infos to display 
{
	char * message; // display message
	size_t posX;
	size_t posY;
	FontDef *font;	
} tsDisplayConfig;

//
typedef struct Menu // 
{
    teMenu menuLevel;
	tsDisplayConfig * displayCnfg;
	
	void (*functBtnUp)(void); // pointer to action by Press ButtUp
	void (*functBtnDwn)(void); // pointer to action by Press ButtDwn
	void (*functBtnLft)(void); // pointer to action by Press ButtLft
	void (*functBtnRgt)(void); // pointer to action by Press ButtRgt
	void (*functBtnSelect)(void); // pointer to action by Press ButtSelect
	
	struct Menu * menuPrevious;
    struct Menu * menuNext;
	struct Menu * menuHome;
	struct Menu * menuSub;
	
	size_t sizeMenu;
	
} tsMenu;


void menuBtnOn(void);
void menuBtnSlct(void);


void ButtonOnEvent(teButtons buttonNum); // Event handler for button_OnHandle semaphore 
void ButtonSlctEvent(void); // Event handler for button_SlctHandle semaphore





#endif // MENU_H
#include "base.h"
#include "stdlib.h"

// Name of the Items of Interface
// 0. Up menu
char X0[]={"Main Menu"};  // 00
char X1[]={"Diagnostic"};  // 01
char X2[]={"Calibration"}; // 02
char X3[]={"Service"};     // 03
//----------------------------------------

// 01. Up menu - Diagnostic menu
char X4[]={"Start"};           // 011
char X5[]={"Repeat"};          // 012
char X6[]={"Diagnostic DONE"}; // 013
//-----------------------------------------


// 02. Up menu - Calibration menu
char X7[]={"Start Calibration"};   // 021
char X8[]={"Stop Calibration"};    // 022
char X9[]={"Calibration is DONE"}; // 023
//-----------------------------------------

// 03. Up menu - Service menu
char X10[]={"Sensors"};    // 031
char X11[]={"Firmware"};   // 032
//-----------------------------------------

// 031. Up menu - Service menu - Sensors
char X12[]={"Sensor_1"};           // 0311
char X13[]={"Sensor_2"};           // 0312
char X14[]={"Sensor_3"};           // 0313
char X15[]={"Sensor_4"};           // 0314
char X16[]={"Sensor_5"};           // 0315
char X17[]={"Sensor_6"};           // 0316
char X18[]={"ADC Gain"};           // 0317
char X19[]={"Measurement Time"};   // 0318
//-----------------------------------------

// ALL menu
char X20[]={"UP"};
char X21[]={"Down"};
char X22[]={"Slct"};
char X23[]={"Back"};

// 031(1-6)
char X24[]={"RED_DATA"};
char X25[]={"GREEN_DATA"};
char X26[]={"BLUE_DATA"};
char X27[]={"CLEAR_DATA"};

//
char X28[]={"COEFF RED"};
char X29[]={"COEFF GREEN"};
char X30[]={"COEFF BLUE"};
char X31[]={"COEFF CLEAR"};

char X32[]={"160msec"};
char X33[]={"320msec"};
char X34[]={"640msec"};

char X35[]={"RGBC"};

// tempplate for position of message and related font 
tsDisplayConfig displayCnfgTmpl[] = 
{
	//mass posX posY Font
	{X0, 0, 	11, &Font_7x10}, // 0. Up menu: Main									//	0
	{X1, 0, 	11, &Font_7x10}, // 0. Up menu: Diagnostic								//	1
	{X2, 0, 	11, &Font_7x10}, // 0. Up menu: Calibration								//	2
	{X3, 0, 	11, &Font_7x10}, // 0. Up menu: Service									//	3
	
	{X4, 0, 	11, &Font_7x10}, // 1. Diagnostic: Start								//	4
	{X5, 0, 	11, &Font_7x10}, // 1. Diagnostic: Repeat								//	5
	{X6, 0, 	11, &Font_7x10}, // 1. Diagnostic: Diagnostic Done						//	6
	
	{X7, 0, 	11, &Font_7x10}, // 2. Calibration: Start Calibration					//	7
	{X8, 0, 	11, &Font_7x10}, // 2. Calibration: Stop Calibration					//	8
	{X9, 0, 	11, &Font_7x10}, // 2. Calibration: Calibration is DONE					//	9
	
	{X10, 0, 	11, &Font_7x10}, // 3. Service: Sensors									//	10
	{X11, 0, 	11, &Font_7x10}, // 3. Service: Firmware								//	11
	
	{X12, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 1						//	12
	{X13, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 2						//	13
	{X14, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 3						//	14
	{X15, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 4						//	15
	{X16, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 5						//	16
	{X17, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 6						//	17
	{X18, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors ADC Gain				//	18
	{X19, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors Measurement Time		//	19
	{X32, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 160msec				//	20
	{X33, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 320msec				//	21
	{X34, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors 640msec				//	22
	{X24, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors RED_DATA				//	23
	{X25, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors GREEN_DATA			//	24
	{X26, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors BLUE_DATA				//	25
	{X27, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors CLEAR_DATA			//	26
	{X35, 0, 	11, &Font_7x10}, // 31. Service: Sensors: Sensors RGBC					//	27
	
};

//-------------------------------------------------------------------------------------------
extern tsMenu menuDiag[];
extern tsMenu menuCalib[];
extern tsMenu menuService[];
extern tsMenu menuSensors[];
extern tsMenu menuSenConfig[];
extern tsMenu menuADC[];
extern tsMenu menuMeasTime[];
extern tsMenu menuRGBC[];

tsMenu menuMain[] = 
{
	// menuLevel	*displayCnfg	*functON	*functON	*functON	*functON	*functON	*previous		*next 		*home			*subMenu		sizeMenu
	{E_MAIN, &displayCnfgTmpl[0], menuBtnOn, 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuMain[3], &menuMain[1], &menuMain[0],	&menuMain[0],		4},	//	Main
	{E_MAIN, &displayCnfgTmpl[1], menuBtnOn, 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuMain[0], &menuMain[2], &menuMain[0],	&menuDiag[0],		4},	//	Diagnostic
	{E_MAIN, &displayCnfgTmpl[2], menuBtnOn, 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuMain[1], &menuMain[3], &menuMain[0],	&menuCalib[0],		4},	//	Calibration
	{E_MAIN, &displayCnfgTmpl[3], menuBtnOn, 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuMain[2], &menuMain[0], &menuMain[0],	&menuService[0],	4},	//	Service
};

tsMenu menuDiag[] = 
{
	// menuLevel	*displayCnfg			*functON	*functON	*functON	*functON	*functON	*previous		*next 		*home			*subMenu	sizeMenu
	{E_DIAGNOSTIC, 	&displayCnfgTmpl[4],	menuBtnOn, 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuDiag[2], &menuDiag[1], &menuMain[1],	&menuMain[0],	3},	//	Start
	{E_DIAGNOSTIC, 	&displayCnfgTmpl[5], 	menuBtnOn, 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuDiag[0], &menuDiag[2], &menuMain[1],	&menuMain[0],	3},	//	Repeat
	{E_DIAGNOSTIC,	&displayCnfgTmpl[6], 	menuBtnOn, 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuDiag[1], &menuDiag[0], &menuMain[1],	&menuMain[0],	3},	//	Diagnostic Done
};

tsMenu menuCalib[] = 
{
	// menuLevel	*displayCnfg		*functON	*functON	*functON	*functON	*functON	*previous		*next 		*home			*subMenu		sizeMenu
	{E_CALIBRATION, &displayCnfgTmpl[7], menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuCalib[2], &menuCalib[1], &menuMain[2],	&menuMain[0], 	3},	//	Start Calibration
	{E_CALIBRATION, &displayCnfgTmpl[8], menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuCalib[0], &menuCalib[2], &menuMain[2],	&menuMain[0], 	3},	//	Stop Calibration
	{E_CALIBRATION,	&displayCnfgTmpl[9], menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuCalib[1], &menuCalib[0], &menuMain[2],	&menuMain[0], 	3},	//	Calibration is DONE
};

tsMenu menuService[] = 
{
	// menuLevel	*displayCnfg		*functON	*functON	*functON	*functON	*functON	*previous			*next 			*home			*subMenu		sizeMenu
	{E_SERVICE, &displayCnfgTmpl[10], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuService[1], &menuService[1], &menuMain[3],	&menuSensors[0], 	2},	//	Sensors
	{E_SERVICE, &displayCnfgTmpl[11], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuService[0], &menuService[0], &menuMain[3],	&menuMain[0], 		2},	//	Firmware
};

tsMenu menuSensors[] = 
{
	// menuLevel	*displayCnfg			*functON	*functON	*functON	*functON	*functON	*previous			*next 			*home			*subMenu	sizeMenu
	{E_SENSORS, 	&displayCnfgTmpl[12], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSensors[5], &menuSensors[1], &menuMain[3],	&menuSenConfig[0], 	6},	//	Sensors 1
	{E_SENSORS, 	&displayCnfgTmpl[13], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSensors[0], &menuSensors[2], &menuMain[3],	&menuSenConfig[0], 	6},	//	Sensors 2
	{E_SENSORS, 	&displayCnfgTmpl[14], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSensors[1], &menuSensors[3], &menuMain[3],	&menuSenConfig[0], 	6},	//	Sensors 3
	{E_SENSORS, 	&displayCnfgTmpl[15], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSensors[2], &menuSensors[4], &menuMain[3],	&menuSenConfig[0], 	6},	//	Sensors 4
	{E_SENSORS, 	&displayCnfgTmpl[16], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSensors[3], &menuSensors[5], &menuMain[3],	&menuSenConfig[0], 	6},	//	Sensors 5
	{E_SENSORS, 	&displayCnfgTmpl[17], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSensors[4], &menuSensors[0], &menuMain[3],	&menuSenConfig[0], 	6},	//	Sensors 6
};

tsMenu menuSenConfig[] = 
{
	// menuLevel			*displayCnfg		*functON	*functON	*functON	*functON	*functON	*previous			*next 				*home			*subMenu	sizeMenu
	{E_SENSORS_CONFIG, 	&displayCnfgTmpl[18], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSenConfig[2], &menuSenConfig[1], &menuMain[3],	&menuADC[0], 		3},	//	Sensors ADC Gain
	{E_SENSORS_CONFIG, 	&displayCnfgTmpl[19], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSenConfig[0], &menuSenConfig[2], &menuMain[3],	&menuMeasTime[0], 	3},	//	Sensors Measurement Time
	{E_SENSORS_CONFIG,	&displayCnfgTmpl[27], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuSenConfig[1], &menuSenConfig[0], &menuMain[3],	&menuRGBC[0], 		3},	//	Sensors RGBC
};


tsMenu menuADC[] = 
{
	// menuLevel	*displayCnfg			*functON	*functON	*functON	*functON	*functON	*previous		*next 		*home		*subMenu	sizeMenu
	{E_ADC_GAIN, 	&displayCnfgTmpl[18], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuADC[2], &menuADC[1], &menuMain[3],	&menuMain[0], 	3},	//	Sensors ADC Gain
	{E_ADC_GAIN, 	&displayCnfgTmpl[18], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuADC[0], &menuADC[2], &menuMain[3],	&menuMain[0], 	3},	//	Sensors ADC Gain
	{E_ADC_GAIN, 	&displayCnfgTmpl[18], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuADC[1], &menuADC[0], &menuMain[3],	&menuMain[0], 	3},	//	Sensors ADC Gain
};

tsMenu menuMeasTime[] = 
{
	// menuLevel			*displayCnfg			*functON	*functON	*functON	*functON	*functON	*previous			*next 			*home			*subMenu	sizeMenu
	{E_MEASUREMENT_TIME, 	&displayCnfgTmpl[20], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuMeasTime[2], &menuMeasTime[1], &menuMain[3],	&menuMain[0], 	3},	//	Sensors Measurement Time 160msec
	{E_MEASUREMENT_TIME, 	&displayCnfgTmpl[21], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuMeasTime[0], &menuMeasTime[2], &menuMain[3],	&menuMain[0], 	3},	//	Sensors Measurement Time 320msec
	{E_MEASUREMENT_TIME, 	&displayCnfgTmpl[22], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuMeasTime[1], &menuMeasTime[0], &menuMain[3],	&menuMain[0], 	3},	//	Sensors Measurement Time 640msec
};

tsMenu menuRGBC[] = 
{
	// menuLevel	*displayCnfg		*functON	*functON	*functON	*functON	*functON	*previous		*next	 	*home			*subMenu	sizeMenu
	{E_RGBC, 	&displayCnfgTmpl[23], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuRGBC[3], &menuRGBC[1], &menuMain[3],	&menuMain[0], 	4},	//	Sensors RED_DATA
	{E_RGBC, 	&displayCnfgTmpl[24], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuRGBC[0], &menuRGBC[2], &menuMain[3],	&menuMain[0], 	4},	//	Sensors GREEN_DATA
	{E_RGBC, 	&displayCnfgTmpl[25], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuRGBC[1], &menuRGBC[3], &menuMain[3],	&menuMain[0], 	4},	//	Sensors BLUE_DATA
	{E_RGBC, 	&displayCnfgTmpl[26], 	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	menuBtnOn,	&menuRGBC[2], &menuRGBC[0], &menuMain[3],	&menuMain[0], 	4},	//	Sensors CLEAR_DATA
};





//------------------------------------------------------------------------------
// prototype
static void svUpdateMenu(tsMenu * pMenu);

// variables

static tsMenu * pCurrentMenu = &menuMain[0];
//static teMenu stateMenuLevel = E_MAIN;





static void svUpdateMenu(tsMenu * pMenu) // displaying
{
	// 
	// add buffer to print out collected info at different positions
	
	
	// set cursor position X; set cursor position Y
	ssd1306_SetCursor(pMenu->displayCnfg->posX, pMenu->displayCnfg->posY);
	
	// set message to display; set font to the message
	ssd1306_WriteString(pMenu->displayCnfg->message, *pMenu->displayCnfg->font);
	
	ssd1306_UpdateScreen();
	ssd1306_Clear();
}





// API
void menuBtnOn(void) // template handler
{
	
}

void menuBtnSlct(void) // template handler
{
	
}


void ButtonOnEvent(teButtons buttonNum)
{
	// buttonNum = E_BUTTON_DWN; //
	
	tsMenu *pPosMenu;
	
	switch (buttonNum)
	{
	  case E_BUTTON_DWN:
		pPosMenu = pCurrentMenu->menuNext;
		break;
		
	  case E_BUTTON_UP:
		pPosMenu = pCurrentMenu->menuPrevious;
		break;
		
	  case E_BUTTON_LFT:
		pPosMenu = pCurrentMenu->menuHome;
		break;
		
	  case E_BUTTON_RGHT:
		pPosMenu = pCurrentMenu->menuSub;
		break;
		
	  default:
		pPosMenu = &menuMain[0];
		break;
	}
	
	svUpdateMenu(pPosMenu);
	pCurrentMenu = pPosMenu;
}



void ButtonSlctEvent(void)
{
	tsMenu *pPosMenu;
	
	pPosMenu = pCurrentMenu->menuSub;
	svUpdateMenu(pPosMenu);
	pCurrentMenu = pPosMenu;
	
	
#if 0
	ssd1306_SetCursor(0, 11); // x; y
	ssd1306_WriteString("Button Select", Font_7x10);
	ssd1306_UpdateScreen();
	ssd1306_Clear();
#endif
}
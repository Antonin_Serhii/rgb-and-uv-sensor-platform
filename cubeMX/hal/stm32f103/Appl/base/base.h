#ifndef _DATA_BASE_H
#define _DATA_BASE_H

#include "main.h"
#include "cmsis_os.h"

#include "stdbool.h"

// module libs
#include "ssd1306.h"
#include "tca9548a.h"
#include "menu.h"

//
extern I2C_HandleTypeDef hi2c2;
extern osSemaphoreId sensor_OnHandle;
extern osSemaphoreId button_OnHandle;
extern osSemaphoreId button_SlctHandle;

// Standard type definitions
typedef char            tCHAR;
typedef unsigned char   tUCHAR;

typedef int8_t          tINT8;
typedef uint8_t         tUINT8;

typedef int16_t         tINT16;
typedef uint16_t        tUINT16;

typedef int32_t         tINT32;
typedef uint32_t        tUINT32;
typedef uint64_t        tUINT64;

typedef float           tFLOAT;

typedef tUINT32         tTIME;

typedef tUINT8          tBIT_FLAGS_8;
typedef tUINT16         tBIT_FLAGS_16;
typedef tUINT32         tBIT_FLAGS_32;
typedef tUINT64         tBIT_FLAGS_64;

typedef void tVOID;
typedef bool tBOOL;

// Common function pointer or callback definitions
typedef tVOID (* tFUNCTION)(tVOID);         // Pointer to a function.
typedef tVOID (* tISR_FUNCTION)(tVOID);     // Pointer to an ISR function.
typedef tVOID (* tFUNCTION_STATUS_CHANGED)(tBOOL);         // Pointer to a function with boolean argument.


// Bit definitions
#define BIT_0                       (0x0001)
#define BIT_1                       (0x0002)
#define BIT_2                       (0x0004)
#define BIT_3                       (0x0008)
#define BIT_4                       (0x0010)
#define BIT_5                       (0x0020)
#define BIT_6                       (0x0040)
#define BIT_7                       (0x0080)
#define BIT_8                       (0x0100)
#define BIT_9                       (0x0200)
#define BIT_10                      (0x0400)
#define BIT_11                      (0x0800)
#define BIT_12                      (0x1000)
#define BIT_13                      (0x2000)
#define BIT_14                      (0x4000)
#define BIT_15                      (0x8000)
#define BIT_16                      (0x00010000L)
#define BIT_17                      (0x00020000L)
#define BIT_18                      (0x00040000L)
#define BIT_19                      (0x00080000L)
#define BIT_20                      (0x00100000L)
#define BIT_21                      (0x00200000L)
#define BIT_22                      (0x00400000L)
#define BIT_23                      (0x00800000L)
#define BIT_24                      (0x01000000L)
#define BIT_25                      (0x02000000L)
#define BIT_26                      (0x04000000L)
#define BIT_27                      (0x08000000L)
#define BIT_28                      (0x10000000L)
#define BIT_29                      (0x20000000L)
#define BIT_30                      (0x40000000L)
#define BIT_31                      (0x80000000L)
#define BIT_32                      (0x0000000100000000LL)
#define BIT_33                      (0x0000000200000000LL)
#define BIT_34                      (0x0000000400000000LL)
#define BIT_35                      (0x0000000800000000LL)
#define BIT_36                      (0x0000001000000000LL)
#define BIT_37                      (0x0000002000000000LL)
#define BIT_38                      (0x0000004000000000LL)
#define BIT_39                      (0x0000008000000000LL)
#define BIT_40                      (0x0000010000000000LL)
#define BIT_41                      (0x0000020000000000LL)
#define BIT_42                      (0x0000040000000000LL)
#define BIT_43                      (0x0000080000000000LL)
#define BIT_44                      (0x0000100000000000LL)
#define BIT_45                      (0x0000200000000000LL)
#define BIT_46                      (0x0000400000000000LL)
#define BIT_47                      (0x0000800000000000LL)
#define BIT_48                      (0x0001000000000000LL)
#define BIT_49                      (0x0002000000000000LL)
#define BIT_50                      (0x0004000000000000LL)
#define BIT_51                      (0x0008000000000000LL)
#define BIT_52                      (0x0010000000000000LL)
#define BIT_53                      (0x0020000000000000LL)
#define BIT_54                      (0x0040000000000000LL)
#define BIT_55                      (0x0080000000000000LL)
#define BIT_56                      (0x0100000000000000LL)
#define BIT_57                      (0x0200000000000000LL)
#define BIT_58                      (0x0400000000000000LL)
#define BIT_59                      (0x0800000000000000LL)
#define BIT_60                      (0x1000000000000000LL)
#define BIT_61                      (0x2000000000000000LL)
#define BIT_62                      (0x4000000000000000LL)
#define BIT_63                      (0x8000000000000000LL)


#pragma pack(1)


#pragma pack()


#endif // DATA_BASE_H
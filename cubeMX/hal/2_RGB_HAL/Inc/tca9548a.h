#ifndef tca9548_I2C
#define tca9548_I2C              hi2c2
#endif

#define tca9548_I2C_adrr  0x70

extern I2C_HandleTypeDef tca9548_I2C;

void switchI2cTo(uint8_t);
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

#include "APDS9960.h"
#include "tca9548a.h"
//-------------------------------------------------------------------------------

void APDS9960_init()
{    
    int i;
    for (i = 0; i < 2; i++)
    {
    switchI2cTo(i);
    APDS9960_write(APDS9960_ATIME, DEFAULT_ATIME);
    APDS9960_write(APDS9960_CONTROL, DEFAULT_AGAIN);
    APDS9960_write(APDS9960_ENABLE, (APDS9960_PON | APDS9960_AEN));
    }
}
//------------------------------------------------------------------------------

uint8_t APDS9960_read(uint8_t addr)
{
    uint8_t data = 0;
    HAL_I2C_Mem_Read(&APDS9960_I2C, APDS9960_I2C_ADDR<<1, addr, 1, &data, 1, APDS9960_I2C_TIMEOUT);
    return data;
}
//------------------------------------------------------------------------------
void APDS9960_write(uint8_t addr, uint8_t data)
{
    uint8_t buf[2] = {addr, data};
    while(HAL_OK != HAL_I2C_Master_Transmit(&APDS9960_I2C, APDS9960_I2C_ADDR<<1, buf, 2, APDS9960_I2C_TIMEOUT));

}
//------------------------------------------------------------------------------

void itoa (int n, char *pbuff) 
{ 
    int i;
    int sign;
    int j; 
    char c;
    
    if ((sign = n) < 0) {
        n = -n;
    }
    
    i = 0;
    
    do { 
        pbuff[i++] = n % 10 + '0'; 
    } while ((n /= 10) > 0);
    
    if (sign < 0) {
        pbuff[i++] = '-'; 
    }
    
    pbuff[i] = '\0'; 
    j = i-1;
    
    for (i = 0; i < j; j=j ) {
        c = pbuff[i];
        pbuff[i] = pbuff[j];
        pbuff[j] = c; 
        i++; j--;
    }
} 
uint8_t  Colour_Clear(uint16_t sensorNamber) 
  {
      switchI2cTo(sensorNamber);
    return  ((APDS9960_read(APDS9960_CDATAH) << 8) + APDS9960_read(APDS9960_CDATAL));
  }
uint8_t  Colour_Red(uint16_t sensorNamber) 
  {
    switchI2cTo(sensorNamber);
    return  ((APDS9960_read(APDS9960_RDATAH) << 8) + APDS9960_read(APDS9960_RDATAL));
  }
uint8_t  Colour_Green(uint16_t sensorNamber) 
  {
      switchI2cTo(sensorNamber);
    return  (APDS9960_read(APDS9960_GDATAH) << 8) + APDS9960_read(APDS9960_GDATAL);
  }
uint8_t  Colour_Blue(uint16_t sensorNamber) 
  {
      switchI2cTo(sensorNamber);
    return  (APDS9960_read(APDS9960_BDATAH) << 8) + APDS9960_read(APDS9960_BDATAL);
  }
//------------------------------------------------------------------------------

/* CODE END Public functions */

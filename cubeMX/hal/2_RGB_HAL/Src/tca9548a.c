/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "tca9548a.h"

void switchI2cTo(uint8_t I2cNamber)
{
    uint8_t buf = 0x1;
    buf = buf << I2cNamber;
    while(HAL_OK != HAL_I2C_Master_Transmit(&tca9548_I2C, tca9548_I2C_adrr << 1, &buf, 1, 1000));
    HAL_Delay(10);
}